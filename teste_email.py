from  airflow import DAG
import pandas as pd
from airflow.operators.dummy_operator import DummyOperator
from airflow.operators.python_operator import PythonOperator
from airflow.operators.bash_operator import BashOperator
from airflow.hooks.postgres_hook import PostgresHook
from airflow.operators.email_operator import EmailOperator
from airflow.utils.trigger_rule import TriggerRule
from airflow.models import Variable
from datetime import datetime, timedelta

#============================================================================================
# Default Arguments
#============================================================================================
default_args = {
    'owner': 'owner',
    'start_date': datetime(2022, 6, 27),
    'retries': 3,
    'retry_delay': timedelta(minutes=5),
}

#============================================================================================
# DAG Settings
#============================================================================================
with DAG(

    dag_id='dag_email_send',
    description='Teste de envio de e-mail',
    default_args=default_args,
    schedule_interval="0 8 * * *",
    dagrun_timeout=timedelta(minutes=60),
    tags=['EMAIL']

) as dag:

# Marca o início da Pipeline
    task_start = DummyOperator(task_id='task_start', trigger_rule=TriggerRule.ALL_SUCCESS)

    # Envia E-mail contendo o arquivo de Logs em anexo, e no corpo do E-mail.
    task_send_email = EmailOperator(
        task_id='send_email',
        to=["analytics@netshow.me"],
        subject="TESTE ENVIO E-MAILS",
        html_content="<h3> Hello Edu! </h3>",
        trigger_rule=TriggerRule.ALL_SUCCESS,
        dag = dag
    )

    # Marca o fim da Pipeline
    task_finish = DummyOperator(task_id='task_finish', trigger_rule=TriggerRule.ALL_SUCCESS)

    # DAG Workflow
    task_start >> task_send_email >> task_finish